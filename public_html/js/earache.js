/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */;

var EarAche = function() {
   
   return {
    game: {},
    
    AUDIO_PATH: 'assets/audio/',
    AUDIO_COPIES: 4,
    
    SINGLE_MODE: 'Single',
    PROGRESSION_MODE: 'Progression',
    PLAY_ALONG_MODE: 'Play Along',


    TO_GUESS_FRAME: 0,
    INACTIVE_FRAME: 1,
    GUESSED_FRAME: 2,
    INCORRECT_FRAME: 3,
    
    TOGGLE_ACTIVE_FRAME: 0,
    TOGGLE_INACTIVE_FRAME: 1,
    
    
    major_row: {},
    minor_row: {},
    seven_row: {},
    
    MAJOR_CHORDS: [{row: null, tag: 'a-major', title: 'A', audio: [], melodic_audio: null, active: true, enabled: true},
            {row: null, tag: 'b-major', title: 'B', audio: [], melodic_audio: null, active: true, enabled: true},
            {row: null, tag: 'c-major', title: 'C', audio: [], melodic_audio: null, active: true, enabled: true},
            {row: null, tag: 'd-major', title: 'D', audio: [], melodic_audio: null, active: true, enabled: true},
            {row: null, tag: 'e-major', title: 'E', audio: [], melodic_audio: null, active: true, enabled: true},
            {row: null, tag: 'f-major', title: 'F', audio: [], melodic_audio: null, active: true, enabled: true},
            {row: null, tag: 'g-major', title: 'G', audio: [], melodic_audio: null, active: true, enabled: true}],
    MINOR_CHORDS: [{row: null,tag: 'a-minor', title: 'Am', audio: [], melodic_audio: null, active: true, enabled: true},
            {row: null, tag: 'b-minor', title: 'Bm', audio: [], melodic_audio: null, active: true, enabled: true},
            {row: null, tag: 'c-sharp-minor', title: 'C#m', audio: [], melodic_audio: null, active: true, enabled: true},
            {row: null, tag: 'd-minor', title: 'Dm', audio: [], melodic_audio: null, active: true, enabled: true},
            {row: null, tag: 'e-minor', title: 'Em', audio: [], melodic_audio: null, active: true, enabled: true},
            {row: null, tag: 'f-sharp-minor', title: 'F#m', audio: [], melodic_audio: null, active: true, enabled: true},
            {row: null, tag: 'g-sharp-minor', title: 'G#m', audio: [], melodic_audio: null, active: true, enabled: true}],
    SEVEN_CHORDS: [{row: null, tag: 'a-major-7', title: 'A7',audio: [], melodic_audio: null, active: true, enabled: true},
            {row: null, tag: 'b-major-7', title: 'B7', audio: [], melodic_audio: null, active: true, enabled: true},
            {row: null, tag: 'c-major-7', title: 'C7', audio: [], melodic_audio: null, active: true, enabled: true},
            {row: null, tag: 'd-major-7', title: 'D7', audio: [], melodic_audio: null, active: true, enabled: true},
            {row: null, tag: 'e-major-7', title: 'E7', audio: [], melodic_audio: null, active: true, enabled: true},
            {row: null, tag: 'f-major-7', title: 'F7', audio: [], melodic_audio: null, active: true, enabled: true},
            {row: null, tag: 'g-major-7', title: 'G7', audio: [], melodic_audio: null, active: true, enabled: true}], 
    isArray: function(value){
      return Object.prototype.toString.apply(value) === '[object Array]';  
    },
    msg: '',
    timer: {
        //60sec/min * 1000ms/sec * 5min
        length: 30000
    },
    flash_sprite: function(sprite,to_flash,length){
        var current_frame = sprite.frame;

        var disable_flash = function(sprite,current_frame){
            return function (){
                sprite.frame = current_frame;  
            };
        }(sprite,current_frame);
        sprite.frame = to_flash;
        this.game.time.events.add(length,disable_flash,this);
    },

    single: { current_chord: ''},
    progression: {
        current_sprites: [],
        current_chords: [],
        current_labels: [],
        to_guess: 0
    },
    play_along: {
        game: {},
        BASIC_FRAME: 0,
        INACTIVE_FRAME: 1,
        FLASH_FRAME: 2
    },
    metronome: {
        ENABLED_FRAME: 0,
        DISABLED_FRAME: 1,
        FLASH_FRAME: 2
    },
    earache: function (){
    
    
    var game = new Phaser.Game(800, 600, Phaser.CANVAS, 'earache-game', { preload: preload, create: create, update: update });
    EarAche.game = game;
    EarAche.play_along.game = game;
    var tabs = [ {tab: EarAche.SINGLE_MODE,
                  enable: enableSingleTab(),
                  disable: disableSingleTab()}
                ,{tab: EarAche.PROGRESSION_MODE,
                  enable: enableProgressionTab(),
                  disable: disableProgressionTab()}
                ,{tab: EarAche.PLAY_ALONG_MODE,
                  enable: enablePlayAlongTab(),
                  disable: disablePlayAlongTab()}];
    
    var chord_buttons = [];
    
    function preload(){
        
        game.load.spritesheet('button','assets/sprites/play_button_sprite.png',300,200);
        game.load.spritesheet('listen_button','assets/sprites/listen_button_sprite.png',200,200);
        game.load.spritesheet('progression_button','assets/sprites/progression_sprite.png',200,200);
        game.load.spritesheet('toggle','assets/sprites/toggle_sprite.png',400,200);
        game.load.spritesheet('metronome_background','assets/sprites/metronome_background.png',450,150);

        game.load.spritesheet('bigtic','assets/sprites/metronome_bigtic_sprite.png',200,200);
        game.load.spritesheet('tic','assets/sprites/metronome_tic_sprite.png',200,200);
        game.load.spritesheet('toc','assets/sprites/metronome_toc_sprite.png',200,200);

        loadChordAssets(EarAche.MAJOR_CHORDS,EarAche.AUDIO_COPIES);
        loadChordAssets(EarAche.MINOR_CHORDS,EarAche.AUDIO_COPIES);
        loadChordAssets(EarAche.SEVEN_CHORDS,EarAche.AUDIO_COPIES);


        game.load.audio('timer1',['assets/audio/timer1.ogg','assets/audio/timer1.mp3']);

        game.load.audio('bigtic',['assets/audio/bigtic.ogg','assets/audio/bigtic.mp3']);
        game.load.audio('tic',['assets/audio/tic.ogg','assets/audio/tic.mp3']);
        game.load.audio('toc',['assets/audio/toc.ogg','assets/audio/toc.mp3']);

   }
    
    //load the audio assets for a set of chords
    //loads multiple copies of chord recordings for variety
    function loadChordAssets(chord_list,copies){
        var ogg_file = '';
        var mp3_file = '';
        
        for(var i = 0; i < chord_list.length; i++){
            if(chord_list[i].enabled){
                //load the first copy
                ogg_file = EarAche.AUDIO_PATH + chord_list[i].tag + '.ogg';
                mp3_file = EarAche.AUDIO_PATH + chord_list[i].tag + '.mp3';

                game.load.audio(chord_list[i].tag,[ogg_file,mp3_file]);

                //load the other copies of the audio
                for(var j = 1; j < copies; j++){
                    ogg_file = EarAche.AUDIO_PATH + chord_list[i].tag + j.toString() + '.ogg';
                    mp3_file = EarAche.AUDIO_PATH + chord_list[i].tag + j.toString() + '.mp3';
                    game.load.audio(chord_list[i].tag + j.toString(),[ogg_file,mp3_file]);
                }

                //load the melodic
                ogg_file = EarAche.AUDIO_PATH + chord_list[i].tag + '-melodic' + '.ogg';
                mp3_file = EarAche.AUDIO_PATH + chord_list[i].tag + '-melodic' + '.mp3';
                game.load.audio(chord_list[i].tag + '-melodic',[ogg_file,mp3_file]);
            }
        }
    }
    
    function create() {
        
        
        //ordinarily Phaser pauses the game if the browser tab loses focus
        //we wish this to keep on going because the timer/metronome are useful
        //to have running in the background
        this.stage.disableVisabilityChange = true;
        
        tabs[0].group = game.add.group();
        tabs[1].group = game.add.group();
        tabs[2].group = game.add.group();
        
        EarAche.backgroundGroup = game.add.group();
        
        var metronome_background = game.add.sprite(155 ,420,'metronome_background',0);
        EarAche.backgroundGroup.add(metronome_background);
        
        game.stage.backgroundColor = 0xffffff;
        
        EarAche.msg = game.add.text(0,160,'');

        EarAche.chords = [].concat(EarAche.MAJOR_CHORDS,EarAche.MINOR_CHORDS,EarAche.SEVEN_CHORDS);
        //create the phaser sounds from the preloaded assets
        createAudioResources(EarAche.AUDIO_COPIES);

        createTabButtons();
        createSingleTab();
        createProgressionTab();
        createPlayAlongTab();
        createChordButtons();

        createTimer();
        
        createMetronome();
    }
    
    function createAudioResources(copies){
        
        for(var i = 0; i < EarAche.chords.length; i++){
            //load the copies of the chord audio
            EarAche.chords[i].audio[0] = game.add.audio(EarAche.chords[i].tag);
            for(var j = 1; j < copies; j++){
                EarAche.chords[i].audio[j] = game.add.audio(EarAche.chords[i].tag + j.toString());
            }
            //just the single melodic to load
            EarAche.chords[i].melodic_audio = game.add.audio(EarAche.chords[i].tag + '-melodic');
        }
    }
    
    function createChordButtons(){
        var xpos = 0;
        var ypos = 210;
        var row_height = 70;
                
        EarAche.major_row.chords = EarAche.MAJOR_CHORDS;
        EarAche.major_row.active = false;
        EarAche.minor_row.chords = EarAche.MINOR_CHORDS;
        EarAche.minor_row.active = false;
        EarAche.seven_row.chords = EarAche.SEVEN_CHORDS;
        EarAche.seven_row.active = false;
                
        createButtonRow(EarAche.major_row,xpos,ypos);
        createButtonRow(EarAche.minor_row,xpos, ypos + row_height);
        createButtonRow(EarAche.seven_row,xpos, ypos + (row_height * 2));
        
    };
    
    function createButtonRow(row, xpos,ypos){
        var control_offset = 50;
        for(var i = 0; i < row.chords.length; i++){
            var chord = row.chords[i];
            chord.row = row;
            if(chord.enabled){
                chord.active = true;
                chord.row.active = true;
                
                createChordButton(chord,control_offset + xpos + (100 * i),ypos);
                createListenButton(chord,control_offset + xpos + (100 * i),ypos);
                createSelectionButton(chord,control_offset +  xpos + (100* i), ypos);
                chord_buttons[i] = chord;
            }
        }
        createRowControl(row,xpos,ypos);
    }
    
    function createSelectionButton(chord, xpos, ypos){
        var y_offset = 0;
        var x_offset = 0;

        chord.selection = game.add.sprite(xpos + x_offset, ypos + y_offset, 'toggle', 0);
        chord.selection.inputEnabled = true;
        chord.selection.scale.setTo(0.10,0.10);

        //buttons are selected by default
        var toggle_action = function(chord) {
            return function(){
                if(!chord.active){
                    chord.selection.frame = EarAche.TOGGLE_ACTIVE_FRAME;
                    chord.button.enable();
                } else {
                    chord.selection.frame = EarAche.TOGGLE_INACTIVE_FRAME;
                    chord.button.disable();
                }
                chord.active = !chord.active;
                setRowToggle(chord.row);
            };
        }(chord);
        chord.selection.events.onInputDown.add(toggle_action,this);
    }

    //if a row's last active button has been disabled or a first button enabled
    //we need to switch the row toggle on/off
    function setRowToggle(row){
        var frame = EarAche.TOGGLE_INACTIVE_FRAME;
        row.active = false;
        for(var i = 0; i < row.chords.length; i++){
            if(row.chords[i].enabled){
                if(row.chords[i].active){
                    frame = EarAche.TOGGLE_ACTIVE_FRAME;
                    row.active = true;
                    break;
                }
            }
        }
        row.sprite.frame = frame;
    }
    
    function createChordButton(chord,xpos, ypos){
        var y_offset = 20;
        var y_text_offset = 10;
        var x_offset = 0;
        var x_text_offset = 10;
        
        var title = chord.title;

        var action = function(chord){
            var chord_title = chord.title;
            return function(){
                var single_action = function(){
                    var current_chord = EarAche.single.current_chord;
                    if(chord_title === current_chord.title){
                        EarAche.single.chord_sprite.frame = EarAche.GUESSED_FRAME;
                        EarAche.single.chord_text.setText(chord_title);
                    } else {
                        EarAche.flash_sprite(EarAche.single.chord_sprite,EarAche.INCORRECT_FRAME,250);
                    }
                };
                var progression_action = function(){
                    var to_guess = EarAche.progression.to_guess;
                    if(to_guess < EarAche.progression.current_chords.length){
                        var chord_to_guess = EarAche.progression.current_chords[to_guess];
                        if(chord_title === chord_to_guess.title){
                            //switch the current sprite to guessed and reveal the title
                            EarAche.progression.current_sprites[to_guess].frame = EarAche.GUESSED_FRAME;
                            EarAche.progression.current_labels[to_guess].setText(chord_title);
                            
                            //if there's an active sprite, make the next sprite active
                            EarAche.progression.to_guess++;
                            if(EarAche.progression.to_guess < EarAche.progression.current_chords.length){
                                EarAche.progression.current_sprites[EarAche.progression.to_guess].frame = EarAche.TO_GUESS_FRAME;
                            }
                        } else {
                            EarAche.flash_sprite(EarAche.progression.current_sprites[to_guess],EarAche.INCORRECT_FRAME,250);
                        }
                    }
                };
                if(chord.active){
                    if(getActiveTab().tab === EarAche.SINGLE_MODE){
                        single_action();
                    } else {
                        progression_action();
                    }
                }
           };
        }(chord);
        
        chord.button = new EarAche.SpriteButton({ 
            xpos: xpos + x_offset, 
            ypos: ypos + y_offset, 
            x_text_offset: x_text_offset, 
            y_text_offset: y_text_offset, 
            xscale: 0.2, 
            yscale: 0.2, 
            textscale: 0.75, 
            text: title, 
            action: action});
    }
    
    function createListenButton(chord, xpos, ypos){
        var y_offset = 0;
        var x_offset = 40;

        var action = function(title){
            return function(){
                playChordByTitle(title);
           };
        }(chord.title);
        chord.listen = game.add.button(xpos + x_offset,ypos + y_offset,'listen_button', action,this, EarAche.BUTTON_OVER, EarAche.BUTTON_OUT,EarAche.BUTTON_DOWN);
        chord.listen.scale.setTo(0.10,0.10);
    }    
    
    function createRowControl(row,xpos,ypos){
       var x_offset = 0;
       var y_offset = 0;
       
       row.sprite = game.add.sprite(xpos + x_offset, ypos + y_offset, 'toggle', 0);
       row.sprite.inputEnabled = true;
       row.sprite.scale.setTo(0.10,0.10);

       var action = function(row){
            return function(){
                if(row.active){
                    row.sprite.frame = EarAche.TOGGLE_INACTIVE_FRAME;
                    for(var i = 0 ; i < row.chords.length; i++){
                        if(row.chords[i].enabled){
                            if(row.chords[i].active){
                                row.chords[i].active = false;
                                row.chords[i].button.disable();
                                row.chords[i].selection.frame = EarAche.TOGGLE_INACTIVE_FRAME;
                            }
                        }
                    }
                } else {
                    row.sprite.frame = EarAche.TOGGLE_ACTIVE_FRAME;
                    for(var i = 0 ; i < row.chords.length; i++){
                        if(row.chords[i].enabled){
                            if(!row.chords[i].active){
                                row.chords[i].active = true;
                                row.chords[i].button.enable();
                                row.chords[i].selection.frame = EarAche.TOGGLE_ACTIVE_FRAME;
                            }
                        }
                    }
                }
                row.active = !row.active;
            };
       }(row);
        row.sprite.events.onInputDown.add(action,this);
 
    }
    function createTabButtons(){
        var xpos = 0;
        var ypos = 0;
        var x_offset = 100;
        var y_offset = 0;
        var x_text_offset = 10;
        var y_text_offset = 10;
     
        var single = new EarAche.SpriteButton({ 
            xpos: xpos, 
            ypos: ypos, 
            xscale: 0.30, 
            yscale: 0.20, 
            textscale: 0.5, 
            text: EarAche.SINGLE_MODE, 
            action: showTab(EarAche.SINGLE_MODE)});

        var progression = new EarAche.SpriteButton({ 
            xpos: xpos + x_offset, 
            ypos: ypos + y_offset, 
            xscale: 0.30, 
            yscale: 0.20, 
            textscale: 0.5, 
            text: EarAche.PROGRESSION_MODE, 
            action: showTab(EarAche.PROGRESSION_MODE)});

        var play_along = new EarAche.SpriteButton({ 
            xpos: xpos + 2 * x_offset, 
            ypos: ypos + 2 * y_offset, 
            xscale: 0.30, 
            yscale: 0.20, 
            textscale: 0.5, 
            text: EarAche.PLAY_ALONG_MODE, 
            action: showTab(EarAche.PLAY_ALONG_MODE)});
    }
    
    function getActiveTab(){
        for(var i = 0; i < tabs.length; i++){
           var tab = tabs[i];
           if(tab.group.visible){
              return tab;
           }
       }
       return null;
    }
    
    function showTab(toShow){
        return function(){
            for(var i = 0; i < tabs.length; i++){
                var tab = tabs[i];
                if(tab.tab === toShow){
                    tab.enable();
                } else {
                    tab.disable();
                }
            }
        };
    };
    
    function enableSingleTab(){
        return function (){
            EarAche.msg.setText('');
            tabs[0].group.visible = true;
        };
    }
    
    function disableSingleTab(){
        return function(){
            tabs[0].group.visible = false;
        };
    }
    
    function createSingleTab(){
        var ypos = 90;
        var y_text_offset = 15;
        var xpos = 0;
        var x_text_offset = 10;
        
        var buttons = [{title: 'Play \nNew', action: playNewSingleChord}
            ,{title: 'Replay', action: playCurrentChord}
            ,{title: 'Melodic', action: playMelodic} ];
        
        for(var i = 0; i < buttons.length; i++){
            var toAdd = new EarAche.SpriteButton({ 
            xpos: xpos + ((120) * i), 
            ypos: ypos, 
            x_text_offset: 10, 
            y_text_offset: 15, 
            xscale: 0.35, 
            yscale: 0.35, 
            textscale: 0.75, 
            text: buttons[i].title, 
            action: buttons[i].action,
            group: tabs[0].group} );
        
       }
        if(!EarAche.single.chord_sprite){
            EarAche.single.chord_sprite = game.add.sprite(xpos + 400, ypos, 'progression_button', EarAche.TO_GUESS_FRAME);
            EarAche.single.chord_sprite.scale.setTo(0.5,0.5);
            EarAche.single.chord_text = game.add.text(xpos + 400 + x_text_offset + 15, ypos + y_text_offset + 15, '?');
            tabs[0].group.add(EarAche.single.chord_sprite);
            tabs[0].group.add(EarAche.single.chord_text);
        } else {
            EarAche.single.chord_sprite.frame = EarAche.TO_GUESS_FRAME;
            EarAche.single.chord_text = '?';
        }
       
       tabs[0].enable();

    }

    function playNewSingleChord(){
        EarAche.single.current_chord = getRandomChord();
        EarAche.single.chord_sprite.frame = EarAche.TO_GUESS_FRAME;
        EarAche.single.chord_text.setText('?');
        playRandomChord();
    }
    
    function enableProgressionTab(){
        return function() {
           EarAche.msg.setText('');
           tabs[1].group.visible = true;
            $('#progression').show();
        };
    }
    
    function disableProgressionTab(){
        return function() {
            tabs[1].group.visible = false;
            $('#progression').hide();
        };
    }
    
    function createProgressionTab(){
        var xpos = 0;
        var ypos = 60;
        var x_display_offset = 400;
        var y_display_offset = 35;
        var x_text_offset = 10;
        var y_text_offset = 15;
        
        var set_button = new EarAche.SpriteButton({ 
            xpos: xpos + 100, 
            ypos: ypos, 
            x_text_offset: x_text_offset, 
            y_text_offset: 5, 
            xscale: 0.2, 
            yscale: 0.12, 
            textscale: 0.5, 
            text: 'Set', 
            action: setProgressionAction(xpos + x_display_offset,ypos + y_display_offset),
            group: tabs[1].group});

        var play_button = new EarAche.SpriteButton({ 
            xpos: xpos, 
            ypos: ypos + 30, 
            x_text_offset: x_text_offset, 
            y_text_offset: y_text_offset, 
            xscale: 0.35, 
            yscale: 0.35, 
            textscale: 0.75, 
            text: 'Play \nNew', 
            action: playNewProgressionAction(xpos + x_display_offset,ypos + y_display_offset),
            group: tabs[1].group});

        var replay_button = new EarAche.SpriteButton({ 
            xpos: xpos + 120, 
            ypos: ypos + 30, 
            x_text_offset: x_text_offset, 
            y_text_offset: y_text_offset, 
            xscale: 0.35, 
            yscale: 0.35, 
            textscale: 0.75, 
            text: 'Replay', 
            action: replayProgressionAction(xpos + x_display_offset,ypos + y_display_offset),
            group: tabs[1].group});
        
        createProgressionForm(xpos,ypos);
        createProgressionDisplay(xpos + x_display_offset, ypos + y_display_offset);
        tabs[1].disable();
    }
    
    function setProgressionAction(xpos,ypos){
        return function(){
            createProgressionDisplay(xpos,ypos);
        };
    }
    
    function getProgressionLength(){
        return parseInt($('#progression_length').val());
    }
    
    function createProgressionForm(xpos,ypos){
        var progression_form = $('#earache-game').append('<form id="progression"></form>');
        $('#progression').append('Length: ');
        $('#progression').append('<input type="text" id="progression_length"></input>');
        $('#progression_length').attr('value','3');
        $('#progression_length').attr('size','1');
        var earache_pos = $('#earache-game').position();
        $('#progression').css({position: 'absolute', top: (earache_pos.top + ypos) + 'px', left: earache_pos.left + xpos + 'px'});
        $('#progression').css('width','100px');
    }
    
    function createProgressionDisplay(xpos,ypos){
        var x_offset = 70;
        var y_offset = 0;
        
        var x_text_offset = 10;
        var y_text_offset = 10;
        
        EarAche.progression.current_sprites = [];
        EarAche.progression.current_labels = [];
        
        //the active chord to guess is the first one
        EarAche.progression.to_guess = 0;
        
        if(!EarAche.progression.display_group){
            EarAche.progression.display_group = game.add.group();
        }
        
        //clear the children but keep the group existing
        EarAche.progression.display_group.destroy(true,true);

        var progression_length = getProgressionLength();

        for(var i = 0; i < progression_length; i++){
            var toAdd;
            var txt;
            if(i === 0 ){
                toAdd = game.add.sprite(xpos + (i*x_offset), ypos, 'progression_button', EarAche.TO_GUESS_FRAME);
            } else {
                toAdd = game.add.sprite(xpos + (i*x_offset), ypos, 'progression_button', EarAche.INACTIVE_FRAME);
            }
            toAdd.scale.setTo(0.3,0.25);
            EarAche.progression.current_sprites[i] = toAdd;
            var txt = game.add.text(xpos + (i*x_offset) + x_text_offset, ypos + y_text_offset, '?');
            EarAche.progression.current_labels[i] = txt;

            EarAche.progression.display_group.add(toAdd);
            EarAche.progression.display_group.add(txt);
        }
        tabs[1].group.add(EarAche.progression.display_group);        
    }

    function playNewProgressionAction(xpos,ypos){
        return function(){
            //refresh the display
            createProgressionDisplay(xpos,ypos);
            //get a new set of chords
            generateRandomProgression();
            playCurrentProgression();
        };
    }
    
    function generateRandomProgression(){
        var random_chords = [];
        var length = getProgressionLength();
        
        for(var i = 0 ; i < length; i++){
            random_chords[i] = getRandomChord();
        }
        EarAche.progression.current_chords = random_chords;
    }
    
    function playCurrentProgression(){
        
        var chord_timer = game.time.create(true);
        for(var i = 0 ; i < EarAche.progression.current_chords.length; i++){
            var toPlay = EarAche.progression.current_chords[i];
            chord_timer.add(1500 * i, playChord,this,toPlay);
        }
        chord_timer.start();
    }
    
    function replayProgressionAction(){
        return function(){
            playCurrentProgression();
        };
    }

    function enablePlayAlongTab(){
        return function() {
            EarAche.msg.setText('');
            tabs[2].group.visible = true;
            $('#play_along').show();

        };
    }
    
    function disablePlayAlongTab(){
        return function() {
            EarAche.play_along.timer.stop();
            tabs[2].group.visible = false;
            $('#play_along').hide();
        };
    }
    
    function createPlayAlongTab(){
        if(!EarAche.play_along.timer){
            EarAche.play_along.timer = game.time.create(false);
        }
        if(EarAche.msg){
            EarAche.msg.setText('');
        }
        var xpos = 0;
        var ypos = 60;
        var x_text_offset = 10;
        var y_text_offset = 15;
        
        var start_button = new EarAche.SpriteButton({ 
            xpos: xpos, 
            ypos: ypos + 30, 
            x_text_offset: x_text_offset, 
            y_text_offset: y_text_offset, 
            xscale: 0.35, 
            yscale: 0.35, 
            textscale: 0.75, 
            text: 'Start', 
            action: startPlayAlong,
            group: tabs[2].group});

        var stop_button = new EarAche.SpriteButton({ 
            xpos: xpos + 120, 
            ypos: ypos + 30, 
            x_text_offset: x_text_offset, 
            y_text_offset: y_text_offset, 
            xscale: 0.35, 
            yscale: 0.35, 
            textscale: 0.75, 
            text: 'Stop', 
            action: stopPlayAlong,
            group: tabs[2].group});

        if(!EarAche.play_along.sprite){
            EarAche.play_along.prior_sprite = game.add.sprite(xpos + 400, ypos + 30,'progression_button', EarAche.play_along.INACTIVE_FRAME);
            EarAche.play_along.prior_sprite.scale.setTo(0.5,0.5);
            tabs[2].group.add(EarAche.play_along.prior_sprite);
            EarAche.play_along.sprite = game.add.sprite(xpos + 510, ypos + 30,'progression_button', EarAche.play_along.FLASH_FRAME);
            EarAche.play_along.sprite.scale.setTo(0.5,0.5);
            tabs[2].group.add(EarAche.play_along.sprite);
            EarAche.play_along.post_sprite = game.add.sprite(xpos + 620, ypos + 30,'progression_button', EarAche.play_along.INACTIVE_FRAME);
            EarAche.play_along.post_sprite.scale.setTo(0.5,0.5);
            tabs[2].group.add(EarAche.play_along.post_sprite);

        }
        if(!EarAche.play_along.chord){
            EarAche.play_along.prior_chord = game.add.text(xpos + 420, ypos + 60,'');
            EarAche.play_along.prior_chord.scale.setTo(1.5);
            tabs[2].group.add(EarAche.play_along.prior_chord);
            EarAche.play_along.chord = game.add.text(xpos + 530, ypos + 60,'');
            EarAche.play_along.chord.scale.setTo(1.5);
            tabs[2].group.add(EarAche.play_along.chord);
            EarAche.play_along.post_chord = game.add.text(xpos + 640, ypos + 60,'');
            EarAche.play_along.post_chord.scale.setTo(1.5);
            tabs[2].group.add(EarAche.play_along.post_chord);
        } else {
            EarAche.play_along.prior_chord.setText('');
            EarAche.play_along.chord.setText('');
            EarAche.play_along.post_chord.setText('');
        }
        createPlayAlongForm(xpos,ypos);

        tabs[2].disable();
    }

    function createPlayAlongForm(xpos,ypos){
        var play_along_form = $('#earache-game').append('<form id="play_along"></form>');
        $('#play_along').append('Chords Per Minute: ');
        $('#play_along').append('<input type="text" id="play_along_speed"></input>');
        $('#play_along_speed').attr('value','60');
        $('#play_along_speed').attr('size','2');
        var earache_pos = $('#earache-game').position();
        $('#play_along').css({position: 'absolute', top: (earache_pos.top + ypos) + 'px', left: earache_pos.left + xpos + 'px'});
    }
    
    function getPlayAlongSpeed(){
        return parseInt($('#play_along_speed').val());
    }
    
    function startPlayAlong(){
        var cpm = getPlayAlongSpeed();
        EarAche.play_along.timer.stop();
        EarAche.play_along.timer.removeAll();
        displayRandomChord();
        if(cpm !== 0){
            EarAche.play_along.timer.loop((60/cpm) * 1000, displayRandomChord,this);
            EarAche.play_along.timer.start();
        }
    }
    
    function stopPlayAlong(){
        EarAche.play_along.timer.stop();
        EarAche.play_along.timer.removeAll();
    }
    
    function displayRandomChord(){
        EarAche.play_along.post_chord.setText(EarAche.play_along.chord.text);
        EarAche.play_along.chord.setText(EarAche.play_along.prior_chord.text);
        EarAche.play_along.prior_chord.setText(getRandomTitle());
//        EarAche.play_along.flash_new(EarAche.play_along.sprite);
    }
    
    function getRandomTitle(){
        var chord = getRandomChord();
        var title = '';
        if(chord !== null){
            title = chord.title;
        }
        return title;
    }
    
    function update() {
    }
    
    function playCurrentChord(){
        playChord(EarAche.single.current_chord);
    }
    
    function playChordByTitle(title){
        for(var i = 0; i < EarAche.chords.length; i++){
            if(title === EarAche.chords[i].title){
                playChord(EarAche.chords[i]);
                break;
            }
        }
    }
    
    function playChord(toPlay){
        if(toPlay){
            var random_index = Math.floor((Math.random() * toPlay.audio.length));
            toPlay.audio[random_index].play();
        }
    }
    
    function getRandomChord(){
        var to_ret = null;
        var active_total = 0;
        //count the number of active chords
        for(var i = 0; i < EarAche.chords.length; i++){
            if(EarAche.chords[i].enabled){
                if(EarAche.chords[i].active){
                    active_total++;
                }
            }
        }
        //if any are active, choose a random one and retrieve it from the list
        if(active_total > 0){
            var random_chord = Math.floor((Math.random() * active_total));
            var curr_active = 0;
            for(var i = 0; i < EarAche.chords.length; i++){
                if(EarAche.chords[i].enabled){
                    if(EarAche.chords[i].active){
                        curr_active++;
                    }
                    if(random_chord + 1 === curr_active){
                        to_ret = EarAche.chords[i];
                        break;
                    }
                }
            }
        }
        return to_ret;
    }
    
    function playMelodic(){
        if(EarAche.single.current_chord){
            EarAche.single.current_chord.melodic_audio.play();
        }
    }
    
    function playRandomChord(){
        EarAche.current_chord = getRandomChord();
        playCurrentChord();
    }
    
    function createTimer(){
        var xpos = 20;
        var ypos = 440;
        
        EarAche.timer.sound = game.add.audio('timer1');
        EarAche.timer.timer = game.time.create(false);
        EarAche.timer.original_min = '5';
        EarAche.timer.original_sec = '00';
        var timer = EarAche.timer.timer;

        var background = game.add.sprite(xpos - 20 ,ypos -20,'progression_button',EarAche.TO_GUESS_FRAME);
        background.scale.setTo(0.75,0.75);
       var title = game.add.text(xpos, ypos,'Timer');
        title.scale.setTo(0.75);
        
        //play
        var play_action = function(timer){

            return function (){           
                timer.stop(true);
                EarAche.timer.original_min = $('#timer_min').val();
                EarAche.timer.original_sec = $('#timer_sec').val();

                var timer_action = function() {
                   var min = parseInt($('#timer_min').val());
                   var sec = parseInt($('#timer_sec').val());
                   decrement_timer(min,sec,timer);
                };
                timer.loop(1000,timer_action,this);
                timer.start();
            };
        }(timer);
        
        var start_b = new EarAche.SpriteButton({ 
            xpos: xpos, 
            ypos: ypos + 50, 
            x_text_offset: 10, 
            y_text_offset: 10, 
            xscale: 0.15, 
            yscale: 0.15, 
            textscale: 0.5, 
            text: 'Start', 
            action: play_action});
        //pause
        var stop_action = function(){
            return function (){
                timer.stop(true);
            };
            
        }();
        var pause_b = new EarAche.SpriteButton({ 
            xpos: xpos + 50, 
            ypos: ypos + 50, 
            x_text_offset: 12, 
            y_text_offset: 10, 
            xscale: 0.15, 
            yscale: 0.15, 
            textscale: 0.5, 
            text: 'Stop', 
            action: stop_action});

        createTimerForm(xpos,ypos + 20);
    }
    
    function createTimerForm(xpos,ypos){
        $('#earache-game').append('<form id="timer"></form>');
        $('#timer').append('<input type="text" id="timer_min"></input>');
        $('#timer_min').attr('value','5');
        $('#timer_min').attr('size','3');
        $('#timer').append(':');
        $('#timer').append('<input type="text" id="timer_sec"></input>');
        $('#timer_sec').attr('value','00');
        $('#timer_sec').attr('size','2');
        var earache_pos = $('#earache-game').position();
        $('#timer').css({position: 'absolute', top: (ypos + earache_pos.top) + 'px', left: xpos + earache_pos.left + 'px'});
    }
    function decrement_timer(min,sec,timer){
        if(min === 0){
            if(sec === 1){
                //time elapsed
                $('#timer_min').val(EarAche.timer.original_min + '');
                $('#timer_sec').val(EarAche.timer.original_sec + '');
                EarAche.timer.sound.play();
                timer.stop(true);

            } else {
                decrement_clock(min,sec);
            }
        } else {
            decrement_clock(min,sec);
        }
    }
    
    function decrement_clock(min,sec){
        if(sec <= 10){
            if(sec === 0){
                $('#timer_sec').val('59');
                $('#timer_min').val(min - 1);
            } else {
                sec = sec - 1;
                $('#timer_sec').val('0' + sec);
            }
        } else {
            sec = sec - 1;
            $('#timer_sec').val(sec);
        }
    }
    function play_sound(sound){
        sound.play();
    }
    
    function fire_metronome(beat_sprite){
        if(beat_sprite){
            if(beat_sprite.frame === EarAche.metronome.ENABLED_FRAME){
                play_sound(beat_sprite.sound);
                EarAche.flash_sprite(beat_sprite,EarAche.metronome.FLASH_FRAME,150);
            }
        } else {
//            EarAche.msg.setText('Encountered null beatsprite');
        }
    }
    
    function createMetronome(){
        var xpos = 170;
        var ypos = 440;
        
        var x_display_offset = 200;
        var y_display_offset = 20;
        
        EarAche.metronome.bigtic = game.add.audio('bigtic');
        EarAche.metronome.tic = game.add.audio('tic');
        EarAche.metronome.toc = game.add.audio('toc');
        EarAche.metronome.beat_group = game.add.group();
        EarAche.metronome.downbeat_group = game.add.group();
        
        var title = game.add.text(xpos, ypos,'Metronome');
        title.scale.setTo(0.75);

        create_metronome_form(xpos,ypos + 20);
        //play
        var set_action = metronome_set_action(xpos + x_display_offset, ypos + y_display_offset);
        new EarAche.SpriteButton({ 
            xpos: xpos, 
            ypos: ypos + 50, 
            x_text_offset: 10, 
            y_text_offset: 10, 
            xscale: 0.15, 
            yscale: 0.15, 
            textscale: 0.5, 
            text: 'Set', 
            action: set_action});
        //play
        var play_action = metronome_play_action();
        new EarAche.SpriteButton({ 
            xpos: xpos + 50, 
            ypos: ypos + 50, 
            x_text_offset: 10, 
            y_text_offset: 10, 
            xscale: 0.15, 
            yscale: 0.15, 
            textscale: 0.5, 
            text: 'Start', 
            action: play_action});

        //pause
        var stop_action = metronome_stop_action();
        new EarAche.SpriteButton({ 
            xpos: xpos + 100, 
            ypos: ypos + 50, 
            x_text_offset: 10, 
            y_text_offset: 10, 
            xscale: 0.15, 
            yscale: 0.15, 
            textscale: 0.5, 
            text: 'Stop', 
            action: stop_action});
        
        EarAche.metronome.metronome = game.time.create(false);

        create_metronome_display(xpos + x_display_offset, ypos + y_display_offset);
    }

    function metronome_set_action(xpos,ypos){
        return function(){
            EarAche.metronome.metronome.stop(true); 
            create_metronome_display(xpos, ypos);
        };
    }
    
    function metronome_play_action(){
        return function (){
            var measure = $('#metronome_measure').val();
            var bpm = $('#metronome_bpm').val();

            var interval = 4000;
            var bpm_val = 1 * bpm;
            var seconds_per_beat = 1;
            if(bpm_val !== 0){
                var seconds_per_beat = 60 / bpm_val;
            }
            interval = measure * seconds_per_beat * 1000;

            EarAche.metronome.metronome.stop(true);

            var add_tic = function(index){
                var beat_sprite = EarAche.metronome.beats[index];
                fire_metronome(beat_sprite);
                EarAche.metronome.metronome.loop(interval,fire_metronome,this,beat_sprite);
            };
            
            var add_toc = function(index){
                var beat_sprite = EarAche.metronome.downbeats[index];
                fire_metronome(beat_sprite);
                EarAche.metronome.metronome.loop(interval,fire_metronome,this,beat_sprite);
            };
            
            for(var i = 0; i < measure; i++){
                EarAche.metronome.metronome.add(seconds_per_beat * 1000 * i, add_tic,this,i);
                EarAche.metronome.metronome.add(seconds_per_beat * 1000 * (i + 0.5), add_toc,this,i);
            }
            EarAche.metronome.metronome.start();
        };
    }
    
    function metronome_stop_action(){
        return function (){
            EarAche.metronome.metronome.stop(true); 
        };
    }
    
    function create_metronome_form(xpos, ypos){
        var metronome_form = $('#earache-game').append('<form id="metronome"></form>');
        $('#metronome').append('Beats: ');
        $('#metronome').append('<input type="text" id="metronome_measure"></input>');
        $('#metronome_measure').attr('value','4');
        $('#metronome_measure').attr('size','1');
        $('#metronome').append(' BPM: ');
        $('#metronome').append('<input type="text" id="metronome_bpm"></input>');
        $('#metronome_bpm').attr('value','60');
        $('#metronome_bpm').attr('size','2');
        var earache_pos = $('#earache-game').position();
        $('#metronome').css({position: 'absolute', top: (earache_pos.top + ypos) + 'px', left: (earache_pos.left + xpos) + 'px'});
        $('#metronome').css('margin-right', '400px');
        $('#metronome').css('width', '195px');
    }
    
    function create_metronome_display(xpos, ypos){
        
        var x_offset = 31;
        var y_offset = 31;
        

        EarAche.metronome.beats = [];
        EarAche.metronome.downbeats = [];
        
        if(!EarAche.metronome.beat_group){
            EarAche.metronome.beat_group = game.add.group();
        }
        if(!EarAche.metronome.downbeat_group){
            EarAche.metronome.downbeat_group = game.add.group();
        }
        EarAche.metronome.beat_group.destroy(true,true);
        EarAche.metronome.downbeat_group.destroy(true,true);
        
        var measure = parseInt($('#metronome_measure').val());

        var toggle_beat = function(beat_sprite){
           return function (){
               if(beat_sprite.frame === EarAche.metronome.ENABLED_FRAME){
                   beat_sprite.frame = EarAche.metronome.DISABLED_FRAME;
               } else {
                   beat_sprite.frame = EarAche.metronome.ENABLED_FRAME;
               }
           };
        };
        for(var i = 0; i < measure; i++){
            var new_tic;
            if( i === 0){
                new_tic = game.add.sprite(xpos + (i*x_offset), ypos, 'bigtic', EarAche.metronome.ENABLED_FRAME);
                new_tic.sound = EarAche.metronome.bigtic;
            } else {
                new_tic = game.add.sprite(xpos + (i*x_offset), ypos, 'tic', EarAche.metronome.ENABLED_FRAME);
                new_tic.sound = EarAche.metronome.tic;
            }
            new_tic.scale.setTo(0.15,0.15);
            new_tic.inputEnabled = true;
            new_tic.events.onInputDown.add(toggle_beat(new_tic),this);
            EarAche.metronome.beat_group.add(new_tic);
            EarAche.metronome.beats[i] = new_tic;
            
            var new_toc = game.add.sprite(xpos + (i*x_offset), ypos + y_offset, 'toc', EarAche.metronome.DISABLED_FRAME);
            new_toc.scale.setTo(0.15,0.15);
            new_toc.inputEnabled = true;
            new_toc.sound = EarAche.metronome.toc;
            new_toc.events.onInputDown.add(toggle_beat(new_toc),this);
            EarAche.metronome.downbeat_group.add(new_toc);
            EarAche.metronome.downbeats[i] = new_toc;
        }
    }
}
};
}();
