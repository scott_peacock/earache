/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

EarAche.SpriteButton = function(args){
    if(!(this instanceof EarAche.SpriteButton)){
        return new EarAche.SpriteButton(args);   
    }
    this.game = EarAche.game;
    this.sprite_key = args.sprite_key;
    if(args.sprite_key === undefined) this.sprite_key = 'button'; 

    this.xpos = args.xpos;
    if(args.xpos === undefined) this.xpos = 0; 
    this.ypos = args.ypos;
    if(args.ypos === undefined) this.ypos = 0; 

    this.x_text_offset = args.x_text_offset;
    if(args.x_text_offset === undefined) this.x_text_offset = 10; 
    this.y_text_offset = args.y_text_offset;
    if(args.y_text_offset === undefined) this.y_text_offset = 10; 

    this.xscale = args.xscale;
    if(args.xscale === undefined) this.xscale = 1; 
    this.yscale = args.yscale;
    if(args.yscale === undefined) this.yscale = 1; 
    this.textscale = args.textscale;
    if(args.textscale === undefined) this.textscale = 1;

    var text = args.text;
    if(text === undefined) text = "";

    this.enabled = args.enabled;
    if(args.enabled === undefined) this.enabled = true;

    this.clickAction = args.action;
    if(args.action === undefined) this.clickAction = function(){};

    this.sprite = this.game.add.sprite(this.xpos,this.ypos,this.sprite_key,this.BUTTON_OUT);
    this.sprite.scale.setTo(this.xscale,this.yscale);
    this.sprite.inputEnabled = true;
    this.sprite.events.onInputDown.add(this.onInputDown,this);
    this.sprite.events.onInputUp.add(this.onInputUp,this);
    this.sprite.events.onInputOver.add(this.onInputOver,this);
    this.sprite.events.onInputOut.add(this.onInputOut,this);
    this.text = this.game.add.text(this.xpos + this.x_text_offset,this.ypos + this.y_text_offset,text);
    this.text.scale.setTo(this.textscale);
   
    if(args.group !== undefined){
        args.group.add(this.sprite);
        args.group.add(this.text);
    }
   
};

EarAche.SpriteButton.prototype = {

    DISABLE_TEXT_COLOR: '#878787',
    DEFAULT_TEXT_COLOR: '#000000',

    BUTTON_OUT: 0,
    BUTTON_DOWN: 1,
    BUTTON_OVER: 2,
    BUTTON_DISABLED: 3,

    clickAction: function (){

    },
    onInputDown: function(){
      if(this.enabled){
          this.sprite.frame = this.BUTTON_DOWN;
      }
    },

    onInputUp: function(){
      if(this.enabled){
        this.sprite.frame = this.BUTTON_OVER;
        this.clickAction();
      }
    },

    onInputOut: function (){
      if(this.enabled){
        this.sprite.frame = this.BUTTON_OUT;
      }
    },

    onInputOver: function(){
      if(this.enabled){
        this.sprite.frame = this.BUTTON_OVER;
      }
    },
    
    disable: function(){
        this.enabled = false;
        this.sprite.frame = this.BUTTON_DISABLED;
        this.text.addColor(this.DISABLE_TEXT_COLOR,0);
    },
    
    enable: function(){
        this.enabled = true;
        this.sprite.frame = this.BUTTON_OUT;
        this.text.addColor(this.DEFAULT_TEXT_COLOR,0);
    }
};
